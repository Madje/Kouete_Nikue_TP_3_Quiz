


//  let questions = `
// [
// 	{
// 		"question":"À quoi sert un aria-label?",
// 		"réponses":[
// 		"Ajouter du contenu textuel sur une balise pour aider les lecteurs d'écran",
// 		"À rien", 
// 		"Je ne sais pas"
// 		], 
// 		"réponse":0
// 	},
// 	{
// 		"question":"HTML vient de :",
// 		"réponses":[
// 			"Hyper Typo Meta Lol",
// 			"Hypertext markup language", 
// 			"Je ne sais pas"
// 		], 
// 		"réponse":1
// 	}
// ]

// `
// questions = JSON.parse(questions)

// function effacerQuestionPrecedente()

// function CreerQuestion()


function AjouterValidations() {
$("form[name='identite']").validate(
  {
    rules: {
      prenoms: "required",
      nom: "required",
      date: {
        required: true,
        // date: true
      }
    },
    message: {
      prenoms: "Veuillez entrer votre prénoms",
      nom: "Veuillez entrer votre nom",
      date: {
        required: "Veuillez entrer votre date de naissance",
        // date: "il doit être sous le forma : AAAA/MM/JJ ",
      }
    },
   submitHandler: function AjouterEvenementPage(){
       // $(".inner").wrap(" <div class='new'</div>")
      $('.questionnaires').hide()
      $('.submit').on('click', function(){
        $('.informations').hide(1000)
        $('.questionnaires').show(1000)
      })
      $('.send').on('click', function(){
        $('.questionnaires').hide(1000)
        $('.score').show(1000)
      })
      $( function() {
        $( "#accordion" ).accordion({
          heightStyle: "content"
        })
         AjouterEvenementPage()
       } );
    },
    showErrors: function(errorMap, errorList) {
      if (est_soumis) {
        var sommaire = "Vous avez les erreurs: \n";
        const ul = $('<ul></ul>')
        $.each(errorList, function () { ul.append(`<li>${this.message}</li>`) })
        $('#sommaire-erreurs').html(ul)
        est_soumis = false;
      }
      this.defaultShowErrors();
    },
    invalidHandler: function(form, validator){
      est_soumis = true;
    }
  });
  AjouterValidations()
}


function AjouterEvenementPage(){
  // $(".inner").wrap(" <div class='new'</div>")
  $('.questionnaires').hide()
  $('.reponses').hide()
  $('#dialog-message').hide()
  $('#reussit').hide()
  $('#echec').hide()

  $('.envoi').on('click', function(){
    $('.informations').hide(1000)
    $('.questionnaires').show(1000)
  })
  $('.send').on('click', function(){
    $('.questionnaires').hide(1000)
    $('.reponses').show(1000)
  })
  $('.end').on('click', function(){
    $('.reponses').hide(1000)

    if( $(`input[name='monster']:checked`).val= 'juste'){
      $('#reussit').hide()
      $('#echec').hide()
      $('#dialog-message').show(1000)
      $( function() {
        $( "#dialog-message" ).dialog({
          modal: true,
          buttons: {
            Ok: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      } )
    } if ($(`input[name='monster']:checked`).val= 'exact'){
      $('#reussit').hide()
      $('#echec').hide()
      $('#dialog-message').show(1000)
      $( function() {
        $( "#dialog-message" ).dialog({
          modal: true,
          buttons: {
            Ok: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      } )
    } if (($(`input[name='monster']:checked`).val= 'juste') && ($(`input[name='monster']:checked`).val= 'exact')){
      $('#reussit').show(1000)
      $('#echec').hide()
      $('#reussit').hide()
      $( function() {
        $( "#reussit" ).dialog({
          modal: true,
          buttons: {
            Ok: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      })
    } else {
      $('#reussit').hide()
      $('#dialog-message').hide()
      $('#echec').show(1000)
      $( function() {
        $( "#echec" ).dialog({
          modal: true,
          buttons: {
            Ok: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      })
    }
   
    $('#reussit').show(1000)
    $( function() {
      $( "#reussit" ).dialog({
        modal: true,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } )

    $('#echec').show(1000)
    $( function() {
      $( "#echec" ).dialog({
        modal: true,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } )
  })
}

$( function() {
  $( "#accordion" ).accordion({
    heightStyle: "content"
  })
  AjouterEvenementPage()
} );

$( function () {
  $('#datatable').DataTable()
} );



//  $( function() {
//   $( "#dialog-message" ).dialog({
//     modal: true,
//     buttons: {
//       Ok: function() {
//         $( this ).dialog( "close" );
//       }
//     }
//   });
// } )



